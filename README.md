## Comment utiliser le projet

* Recupérer le projet en local:     
`git clone git@gitlab.com:exos-api-video/client.git`

* Recupérer les dépendances:     
`composer install`

* Pensez à lancer le docker du projet server

* Les commandes disponibles sont les suivantes:       
`php bin/console app:create-video <name> <duration>`    
`php bin/console app:get-all-videos`    
`php bin/console app:get-one-video <id>`    


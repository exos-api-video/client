<?php 

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use App\Client\VideoClientInterface;

class GetOneVideoCommand extends Command
{
    protected static $defaultName = 'app:get-one-video';

    private $videoClient;

    public function __construct(VideoClientInterface $videoClient)
    {
        $this->videoClient = $videoClient;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Get one video from the server')
            ->setHelp('Get one video from the server on localhost:8080');

        $this->addArgument('id', InputArgument::REQUIRED, 'Id of the video to search for');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        $output->writeln(
            json_encode(
                $this->videoClient->getVideo($id)
            )
        );
    }
}
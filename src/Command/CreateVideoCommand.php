<?php 

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use App\Client\VideoClientInterface;

class CreateVideoCommand extends Command
{
    protected static $defaultName = 'app:create-video';

    private $videoClient;

    public function __construct(VideoClientInterface $videoClient)
    {
        $this->videoClient = $videoClient;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Create one video from the server')
            ->setHelp('Create one video from the server on localhost:8080');
        
        $this->addArgument('name', InputArgument::REQUIRED, 'Name of the video to create');
        $this->addArgument('duration', InputArgument::REQUIRED, 'Duration of the video to create');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $duration = $input->getArgument('duration');
        $output->writeln(
            json_encode(
                $this->videoClient->createVideo($name, $duration)
            )
        );
    }
}
<?php 

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Client\VideoClientInterface;

class GetAllVideosCommand extends Command
{
    protected static $defaultName = 'app:get-all-videos';

    private $videoClient;

    public function __construct(VideoClientInterface $videoClient)
    {
        $this->videoClient = $videoClient;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Get all videos from the server')
            ->setHelp('Get all videos from the server on localhost:8080');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(
            json_encode(
                $this->videoClient->getVideos()
            )
        );
    }
}
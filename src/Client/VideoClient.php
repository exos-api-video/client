<?php 

namespace App\Client;

use GuzzleHttp\Client;

class VideoClient implements VideoClientInterface
{
    const SERVER_URL = 'http://localhost:8080/videos';

    private $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client();
    }

    public function createVideo($name, $duration)
    {
        $response = $this->httpClient->request(
            'POST', 
            self::SERVER_URL,
            [
                'form_params' => [
                    'name' => $name,
                    'duration' => $duration
                ],
                'http_errors' => false
            ]
        );
        return json_decode($response->getBody(), true);
    }

    public function getVideos()
    {
        $response = $this->httpClient->request(
            'GET', 
            self::SERVER_URL,
            [
                'http_errors' => false
            ]
        );
        return json_decode($response->getBody(), true);
    }

    public function getVideo($id)
    {
        $response = $this->httpClient->request(
            'GET', 
            self::SERVER_URL.'/'.$id,
            [
                'http_errors' => false
            ]
        );
        return json_decode($response->getBody(), true);
    }

}
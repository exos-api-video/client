<?php
namespace App\Client;

interface VideoClientInterface {
    /**
     * Create a video
     * 
     * @param string The video name
     * @param int The video duration
     * @return array The array representation of the JSON video
     */
    public function createVideo($name, $duration);
    
    /**
     * Get all videos
     * 
     * @return array The array representation of the JSON video list
     */
    public function getVideos();

    /**
     * Get a video
     * 
     * @return array The array representation of the JSON video 
     */
    public function getVideo($id);
}
